let test1=
{"first_name": "JoHN", "last_name": "SMith"};
let test2 =
{"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"};


let isObject=inp =>
{
  if(inp==null)return false;
  else if(typeof inp == 'object')
  {
    return true;
  }
  return false;
}

let string4=obj =>
{
    if(isObject(obj))
    {
        let first=obj.first_name;
        let middle=obj.middle_name;
        let last=obj.last_name;
        if(first==undefined)first="";
        if(middle==undefined)middle="";
        else middle=middle+" ";
        if(last==undefined)last="";

        let name=first+" "+middle+last;
        return name.toUpperCase();
    }else return "";
}

module.exports=string4;